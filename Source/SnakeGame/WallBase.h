// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "WallBase.generated.h"

class ASnakeElementBase;
class ASnakeBase;
class UStaticMeshComponent;
class USCubeComponent;
class UStaticMesh;

UCLASS()
class SNAKEGAME_API AWallBase : public AActor, public IInteractable
{
	GENERATED_BODY()

public:	
	
	AWallBase();
	
	int32 Score = 0;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
	UStaticMesh* WallComponent;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* WallPiece;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//void WallOverlap();

	//void SetScore();


	//virtual void Interact(AActor* Interactor, bool bIsHead);

};
