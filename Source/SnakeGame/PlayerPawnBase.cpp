﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Food.h"
#include "WallBase.h"
#include "EngineGlobals.h"
#include "SpeedBoosterBase.h"
#include "FoodSlowBase.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Containers/UnrealString.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
    RootComponent = PawnCamera;
	
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	GetGameMode();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	if (GameMode > 0) {
	//сколько времени прошло
	BuferTime += DeltaTime;
	SBuferTime += DeltaTime;
	SlowBufferTime += DeltaTime;
	TimerBuffer += DeltaTime;
	MinutesBuffer += DeltaTime;
	FullTimeBuffer += DeltaTime;
	SecondsCheckBuf += DeltaTime;

	//проверка наличия задержки

		if (FullTimeBuffer < 180.f) {


			if (BuferTime > StepDelay)
			{
				//вызов метода
				AddFoodRandom();
				AddFoodWhiteField();
				//обуление счетчика
				BuferTime = 0;
			}


			if (SBuferTime > SpeedSpawnDelay)
			{
				AddSpeedBoostBlack();
				AddSpeedWhite();
				SBuferTime = 0;
			}

			if (SlowBufferTime > SlowSpawnStep)
			{
				AddSlowBlack();
				AddSlowWhite();
				SlowBufferTime = 0;
			}

			if (TimerBuffer >= Second)
			{
				GameTimer = GameTimer - Second;
				TimerBuffer = 0;

			}

			if (SecondsCheckBuf > 60.f)
			{
				GameTimer = 59;
				SecondsCheckBuf = 0;
			}

			if (MinutesBuffer >= 60.f)
			{
				minutes = minutes - 1;
				MinutesBuffer = 0;
			}
		}
	}
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);

}

void APlayerPawnBase::AddWallBlack()
{
	FRotator StartPointRotation = FRotator(0, 0, 90);

	FVector StartPoint = FVector(WallX, WallY, SpawnZ);

	GetWorld()->SpawnActor<AWallBase>(StartPoint, StartPointRotation);
}

void APlayerPawnBase::AddSpeedWhite()
{
	FRotator StartPointRotation = FRotator(0, 0, 0);

	//Задание случайной координаты по Х через библиотеку FMath
	float SpawnXW = FMath::FRandRange(MinXWhite, MaxXWhite);

	// Аналогичное для Y
	float SpawnYW = FMath::FRandRange(MinYWhite, MaxYWhite);


	FVector StartPoint = FVector(SpawnXW, SpawnYW, SpawnZ);

	GetWorld()->SpawnActor<ASpeedBoosterBase>(StartPoint, StartPointRotation);
}

void APlayerPawnBase::AddSlowWhite()
{
	FRotator StartPointRotation = FRotator(0, 0, 0);

	//Задание случайной координаты по Х через библиотеку FMath
	float SpawnXW = FMath::FRandRange(MinXWhite, MaxXWhite);

	// Аналогичное для Y
	float SpawnYW = FMath::FRandRange(MinYWhite, MaxYWhite);


	FVector StartPoint = FVector(SpawnXW, SpawnYW, SpawnZ);

	GetWorld()->SpawnActor<AFoodSlowBase>(StartPoint, StartPointRotation);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
	GameMode = 1;
}


void APlayerPawnBase::HandlePlayerVerticalInput(float value)

{
	if (IsValid(SnakeActor)) {
		if (value > 0)
		{
			SnakeActor->TrySetCurrentDirection(EMovementDirection::UP);
		}
		else if (value < 0)
		{
			SnakeActor->TrySetCurrentDirection(EMovementDirection::DOWN);
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor)) {
		if (value > 0)
		{
			SnakeActor->TrySetCurrentDirection(EMovementDirection::RIGHT);
		}
		else if (value < 0)
		{
			SnakeActor->TrySetCurrentDirection(EMovementDirection::LEFT);
		}
	}
}

//Тут мы переключаем значение переменной паузы
void APlayerPawnBase::HandlePlayerPauseInput(float value)
{
		if (value == 0)
		{
			GamePause = !GamePause;
		}
}

void APlayerPawnBase::AddFoodWhiteField()
{
	// поворот яблока при спавне
	FRotator StartPointRotation = FRotator(0, 0, 0);

	//Задание случайной координаты по Х через библиотеку FMath
	float SpawnX = FMath::FRandRange(MinXWhite, MaxXWhite);

	// Аналогичное для Y
	float SpawnY = FMath::FRandRange(MinYWhite, MaxYWhite);


	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);

	GetWorld()->SpawnActor<AFood>(StartPoint, StartPointRotation);

}

void APlayerPawnBase::AddSpeedBoostBlack()
{
	FRotator StartPointRotation = FRotator(0, 0, 0);

	//Задание случайной координаты по Х через библиотеку FMath
	float SpawnX = FMath::FRandRange(MinX, MaxX);

	// Аналогичное для Y
	float SpawnY = FMath::FRandRange(MinY, MaxY);


	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);

	GetWorld()->SpawnActor<ASpeedBoosterBase>(StartPoint, StartPointRotation);

}

int APlayerPawnBase::GetFinalScore() const
{
	if (!SnakeActor) { return 0; }
	return SnakeActor->SnakeElements.Num() - SnakeActor->InitialSnakeSize;
}

void APlayerPawnBase::AddSlowBlack()
{
	FRotator StartPointRotation = FRotator(0, 0, 0);

	//Задание случайной координаты по Х через библиотеку FMath
	float SpawnX = FMath::FRandRange(MinX, MaxX);

	// Аналогичное для Y
	float SpawnY = FMath::FRandRange(MinY, MaxY);


	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);

	GetWorld()->SpawnActor<AFoodSlowBase>(StartPoint, StartPointRotation);
}

void APlayerPawnBase::AddFoodRandom()
{
	// поворот яблока при спавне
	FRotator StartPointRotation = FRotator (0, 0, 0);

	//Задание случайной координаты по Х через библиотеку FMath
	float SpawnX = FMath::FRandRange(MinX, MaxX);

	// Аналогичное для Y
	float SpawnY = FMath::FRandRange(MinY, MaxY);


	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);

	GetWorld()->SpawnActor<AFood>(StartPoint, StartPointRotation);

	
}

