// Fill out your copyright notice in the Description page of Project Settings.


#include "BlackSnakeElemBase.h"
#include "Interactable.h"
#include "BlackSnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ABlackSnakeElemBase::ABlackSnakeElemBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABlackSnakeElemBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlackSnakeElemBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

