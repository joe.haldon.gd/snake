﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "WallBase.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Components/BoxComponent.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"


// Sets default values
AWallBase::AWallBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	WallComponent = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'")).Object;
	//UMaterialInstance* SpeedColor;
	FVector Size = FVector(0.5f, 5.f, 5.f);
	WallPiece = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall"));
	WallPiece->SetStaticMesh(WallComponent);
	WallPiece->SetRelativeScale3D(Size);
	
}

// Called when the game starts or when spawned
void AWallBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWallBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// в этом месте функция работает лучше, она съедает всю змею, но все равно делает это постепенно
	//WallOverlap();

}

//void AWallBase::WallOverlap()
//{
//	TArray<AActor*>OverlappedActors;
//	GetOverlappingActors(OverlappedActors);
//
//	for (int32 i = 0; i < OverlappedActors.Num(); ++i)
//	{
//		OverlappedActors[i]->Destroy(true, true);
//		SetScore();
//	}
//}

//void  AWallBase::SetScore()
//{
//	++Score;
//}





// В общем оно компилируется но работает не правильно
//void AWallBase::Interact(AActor* Interactor, bool bIsHead)
//{
//	if (bIsHead)
//
//	{
//	
//		auto Snake = Cast<ASnakeBase>(Interactor);
//		if (IsValid(Snake))
//		{
//			WallOverlap();
//		}
//	}
//}

