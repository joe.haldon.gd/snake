// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "BlackSnakeElemBase.generated.h"

class UStaticMeshComponent;
class ABlackSnakeBase;

UCLASS()
class SNAKEGAME_API ABlackSnakeElemBase : public AActor
{
	GENERATED_BODY()

private:
		int32 BlackIndex;
public:	
	// Sets default values for this actor's properties
	ABlackSnakeElemBase();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY()
		ABlackSnakeBase* BlackSnakeOwner;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;



};
