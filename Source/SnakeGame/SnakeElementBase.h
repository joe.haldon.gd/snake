﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeElementBase.generated.h"

class UStaticMeshComponent;
class ASnakeBase;

UCLASS()
class SNAKEGAME_API ASnakeElementBase : public AActor, public IInteractable

{
	GENERATED_BODY()

private:
	//Индекс каждого нового элемента змеи
	int32 Index;
	
public:	
	// Sets default values for this actor's properties
	ASnakeElementBase();
	UPROPERTY (VisibleAnyWhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	//Указатель на змейку, он позволяет получить к ней внешний доступ
	//Он нужен потому, что именно змейка изначально заспаунила элемент
	//См. SnakeBase, чтобы увидеть значение указателя
	UPROPERTY()
	ASnakeBase* SnakeOwner;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
	void SetFirstElementType();
	void SetFirstElementType_Implementation();
	//устанавливаем индекс нового элемента
	void SetIndex(int32 ElementIndex);
	//получаем индекс индекс нового кусочка 
	int32 GetIndex() const;

	//подключение интефейса Interactable
	//в unrealEngine класс интерфейс встроен в движок и создается из движка путем выбора его из контекстного меню в контенте
	//посмотреть код интерфейса можно в его сорс файлах

	virtual void Interact(AActor* Interactor, bool bIsHead);

	//Функция, которая обрабатывает столкнокения
	//В качестве аргументов тут примитивный компонент, оверлапнутый актор, 
	//Примтитивный компонент другого актора, который вызвал событие оверлапа
	//Остальные аргументы в видео не поясняются а даются с комментарием "Так надо". >:/
	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
							AActor*OtherActro, UPrimitiveComponent*OtherComp, 
							int32 OtherBodyIndex, bool bFromSweep, 
							const FHitResult &SweepResult);

	UFUNCTION()
	void ToggleCollision();
};
