﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class APlayerPawnBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;
	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY()
	int InitialSnakeSize = 4;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum =1);

	UFUNCTION(BlueprintCallable)
	void Move();

	//При событии коллизии вызывается этот метод, который в качестве аргумента принимает элемент, с которым произошла коллизия
	// а также сущность "другой актер" - это тот самый актер с которым сколизился блок
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor *Other);

	UFUNCTION()
	void BoostSpeed(const float SpeedDelta= 0.3);

	UFUNCTION()
	void DecreaseSpeed(const float NegativeSpeedDelta = 0.2f);

	UFUNCTION()
	void DestroyRange(int32 From, int32 To);

	UFUNCTION()
	bool TrySetCurrentDirection(EMovementDirection Direction);

	UFUNCTION()
	EMovementDirection GetCurrentDirection() const;

	UFUNCTION()
	bool IsValidNewDirection(EMovementDirection Direction) const;

private:
	bool HasOppositeDirection(EMovementDirection first, EMovementDirection second) const;

private:
	EMovementDirection LastMoveDirection;
	EMovementDirection CurrentMoveDirection;

};
