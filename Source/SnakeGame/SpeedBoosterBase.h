// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SpeedBoosterBase.generated.h"

class UStaticMeshComponent;
class USphereComponent;
class UStaticMesh;

UCLASS()
class SNAKEGAME_API ASpeedBoosterBase : public AActor, public IInteractable
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASpeedBoosterBase();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
	UStaticMeshComponent* SpeedPiece;

	UPROPERTY(EditDefaultsOnly)
	UStaticMesh* SpeedComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead);

};