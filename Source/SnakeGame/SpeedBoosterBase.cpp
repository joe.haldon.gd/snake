// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBoosterBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include "Interactable.h"
#include "EngineGlobals.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Components/SphereComponent.h"
#include "UObject/ConstructorHelpers.h"



// Sets default values
ASpeedBoosterBase::ASpeedBoosterBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	SpeedComponent = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'")).Object;
	class UMaterialInterface* SpeedColor;
	SpeedColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInterface>(TEXT("Material'/Game/Materials/BasicAsset02.BasicAsset02'")).Get();
	FVector Size = FVector(0.9f, 0.9f, 0.9f);
	SpeedPiece = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Food"));
	SpeedPiece->SetStaticMesh(SpeedComponent);
	SpeedPiece->SetRelativeScale3D(Size);
	SpeedPiece->SetMaterial(0, SpeedColor);
}

// Called when the game starts or when spawned
void ASpeedBoosterBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedBoosterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedBoosterBase::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)

	{
		Destroy();

		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->BoostSpeed();
			GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Blue, FString::Printf(TEXT("Speed overlap")));
		}
	}
}


