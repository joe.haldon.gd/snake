﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeElementBase.h"
#include "SnakeBase.h"
#include "Interactable.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));

	//Задан просчет оверлапов без учета физики и ответ в случае столкновения
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementBase::SetFirstElementType_Implementation()
{
	//Создаем связь с моментом оверлапа, передаем актера по кторому считаем оверлап, а также ссылаемся на связанный метод 
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}

void ASnakeElementBase::SetIndex(int32 ElementIndex) {
	Index = ElementIndex;
}

int32 ASnakeElementBase::GetIndex() const {
	return Index;
}

// Эта имплементация не проверяет является ли элемент, с которым мы столкнулись головой, а просто засчитывает поражение в любом случае
void ASnakeElementBase::Interact(AActor* Interactor, bool bIsHead)
{
	//получаем скастованный указатель на змейку
	auto Snake = Cast<ASnakeBase>(Interactor);

	if (IsValid(Snake))
	{
		// FIXME: Пока кажется сюда нечего положить?
	}
}

//Реализация метода, теперь когда меш получает сигнал о начале оверлапа, то срабатывает вот этот метод
void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
											AActor* OtherActor, 
											UPrimitiveComponent* OtherComp, 
											int32 OtherBodyIndex, 
											bool bFromSweep, 
											const FHitResult& SweepResult)
{
	//проверяем валидность указателя на змейку
	if (IsValid(SnakeOwner))
	{
    // при положительном результате вызывается событие коллизии и метод (см SnakeBase.h)
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
		
	}
}

// функция обрабатывающая столкновение элементов змейки при меремещении
// в случае если захода в себя не было.
void ASnakeElementBase::ToggleCollision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}

	else
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

