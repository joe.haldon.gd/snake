﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Interactable.h"
#include "Components/SphereComponent.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "EngineGlobals.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "UObject/ConstructorHelpers.h"
#include "SnakeElementBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	MyRootComponent = CreateDefaultSubobject<USphereComponent>(TEXT("FoodRoot"));
	RootComponent = MyRootComponent;
	FoodComponent = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'")).Object;
	class UMaterialInterface* FoodColor;
	FoodColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInterface>(TEXT("Material'/Game/Materials/OtherAppleMat.OtherAppleMat'")).Get();

	FVector Size = FVector(0.7f, 0.7f, 0.7f);
	FoodPiece = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Food"));
	FoodPiece->AttachTo(MyRootComponent);
	FoodPiece->SetStaticMesh(FoodComponent);
	FoodPiece->SetRelativeScale3D(Size);
	FoodPiece->SetMaterial(0, FoodColor);

}


// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	TimeGone += DeltaTime;

	if (TimeGone > LifeTime)
	{
		Destroy();
		TimeGone=0;
	}

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)

	{
		Destroy();
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
		

		}
	}
}



