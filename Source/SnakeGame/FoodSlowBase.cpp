// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSlowBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include "Interactable.h"
#include "EngineGlobals.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Components/SphereComponent.h"
#include "UObject/ConstructorHelpers.h"


// Sets default values
AFoodSlowBase::AFoodSlowBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SlowComponent = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'")).Object;
	class UMaterialInterface* SlowColor;
	SlowColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInterface>(TEXT("Material'/Game/Materials/SlowMat.SlowMat'")).Get();
	FVector Size = FVector(0.5f, 0.5f, 0.5f);
	SlowPiece = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Slow"));
	SlowPiece->SetStaticMesh(SlowComponent);
	SlowPiece->SetRelativeScale3D(Size);
	SlowPiece->SetMaterial(0, SlowColor);
}

// Called when the game starts or when spawned
void AFoodSlowBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodSlowBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodSlowBase::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)

	{
		Destroy();

		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->DecreaseSpeed();
			GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Yellow, FString::Printf(TEXT("Speed overlap")));
		}
	}
}

