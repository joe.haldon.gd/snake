﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "WallBase.h"
#include "PlayerPawnBase.generated.h"


class UCameraComponent;
class ASnakeBase;
class AFood;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodActorClass;

	UPROPERTY(BlueprintReadWrite)
		AFood* FoodActor;
	UPROPERTY(BlueprintReadWrite)
		AWallBase* WallActor;

	int32 GameMode = 0;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
		void HandlePlayerVerticalInput(float value);
	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);
	UFUNCTION()
		void HandlePlayerPauseInput(float value);


	float MinXWhite = -1120.f; float MaxXWhite = 890.f;
	float MinYWhite = 60.f; float MaxYWhite = 2000.f;

	//Переменные отвечающие за границы поля
	float MinX = -1120.f; float MaxX = 890.f;
	float MinY = -2140.f; float MaxY = -140.f;
	//Высота спавна яблок в рандомном генераторе
	float SpawnZ = 0.f;
	float WallX = 540;
	float WallY = -670;

	UFUNCTION()
	void AddFoodRandom();

	UFUNCTION()
		void AddFoodWhiteField();
	UFUNCTION()
		void AddSpeedBoostBlack();
	UFUNCTION()
		void AddSlowBlack();
	UFUNCTION()
		void AddWallBlack();
	UFUNCTION()
		void AddSpeedWhite();
	UFUNCTION()
		void AddSlowWhite();
	

	UFUNCTION(BlueprintCallable, Category ="SnakePawn")
		void CreateSnakeActor();

	//Задержка
	float StepDelay = 1.f;
	//Накопитель времени
	float BuferTime = 0;

	// переменные для спавна буста скорости
	float SpeedSpawnDelay = 10.f;
	float SBuferTime= 0.f;
	float SlowSpawnStep = 9.f;
	float SlowBufferTime = 0.f;
	float GameTimer = 60.f;
	float TimerBuffer = 0.f;
	float Second = 1.f;
	float minutes = 2.f;
	float MinutesBuffer = 0.f;
	float FullTimeBuffer = 0.f;
	float SecondsCheck = 60.f;
	float SecondsCheckBuf = 0.f;

	//Задаем перменную пауза и пишем метод, который проверяет ее значение
	bool GamePause = false;

	UFUNCTION(BlueprintCallable, Category = "SnakeBase")
	bool GetGamePause() const {return GamePause; }

    UFUNCTION(BlueprintCallable, Category = "SnakePawn")
	int32 GetGameMode() const { return GameMode; }

	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
	float GetTimer() const { return GameTimer; }

	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
	float GetMinutes() const { return minutes; }

	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
	int GetFinalScore() const;
};
