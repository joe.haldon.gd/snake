﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "Interactable.h"
#include "SnakeElementBase.h"
#include "PlayerPawnBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	LastMoveDirection = EMovementDirection::DOWN;
	CurrentMoveDirection = LastMoveDirection;
	MovementSpeed = 10.f;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(InitialSnakeSize);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
		Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	// в цикл добавлена строчка, поясняющая указатель в хэдере элементов
	for (int i =0; i<ElementsNum;++i)
	{
	FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
	FTransform NewTransform(NewLocation);
	ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
	//значение указателя снейк оунер устанавливается при добавлении нового блока вот тут
	NewSnakeElem->SnakeOwner = this;
	int32 ElementIndex = SnakeElements.Add(NewSnakeElem); 
	NewSnakeElem->SetIndex(ElementIndex);
	
	if (ElementIndex == 0) {
		NewSnakeElem->SetFirstElementType();
	}

	}
}

void ASnakeBase::Move()
{
		// Так как в этом месте происходит перемещение элементов змейки,
		// то можно обновить последнее выбранное направление
		LastMoveDirection = CurrentMoveDirection;
		float MovementSpeedDelta = ElementSize;
		FVector MovementVector(ForceInitToZero);
		switch (CurrentMoveDirection)
		{
		case EMovementDirection::UP:
			MovementVector.X += MovementSpeedDelta;
			break;
		case EMovementDirection::DOWN:
			MovementVector.X -= MovementSpeedDelta;
			break;
		case EMovementDirection::LEFT:
			MovementVector.Y += MovementSpeedDelta;
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y -= MovementSpeedDelta;
			break;
		
	}

	SnakeElements[0]->ToggleCollision();
	//запоминаем предыдущее положение головы змейки и перемещаем остальные элементы по ее траектории
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}


void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor *Other)
{
	if (IsValid(OverlappedElement))
	{
// проверка того, что в коллизию вошла голова змейки
		int32 ElemIndex;
//	В общем тут кроется главная засада, потому что этот кусок функции находит элемент змеи, который вступил в оверлап
// Атакже мы зачем-то вручную записываем сюда индекс элемента, видимо чтобы прописать, что оверлапы считаются по голове.
// Но противоречие теперь в том, что нам надо считать оверлапы и в body
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;

		if (bIsFirst) {
			// Если голова змеи столкнулась с частью себя, то удаляем всё
			// от этого элемента до хвоста змеи
			ASnakeElementBase* BodyElement = Cast<ASnakeElementBase>(Other);
			if (IsValid(BodyElement)) {
				DestroyRange(BodyElement->GetIndex(), SnakeElements.Num() - 1);
				return;
			}
		}

// проверяем подключен ли объект столкновения к интерактивному интерфейсу
//Интересно нельзя ли отсюда вытащить индекс актора с которым произошла коллизия
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
// проверяем что каст успешно состоялся
		if (InteractableInterface)
		{
// если все правильно, то выполняется тот метод, который записан в интерфейсе актора
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::BoostSpeed(const float SpeedDelta)
{
	const auto TickInterval = GetActorTickInterval() * (1 - SpeedDelta);
	SetActorTickInterval(TickInterval);
}

void ASnakeBase::DecreaseSpeed(const float NegativeSpeedDelta)
{
	const auto TickIntervalNeg = GetActorTickInterval() / (1 - NegativeSpeedDelta);
	SetActorTickInterval(TickIntervalNeg);
}

// удаляем часть элементов змейки
void ASnakeBase::DestroyRange(int32 From, int32 To) {
	for(int32 Index=To; Index >= From; --Index) {
		SnakeElements[Index]->Destroy();
		SnakeElements.RemoveAt(Index);
	}
}


// проверяем, что переданные два направления противоположны друг другу. 
// причём их порядок в каком передавать second и first не важен
bool ASnakeBase::HasOppositeDirection(EMovementDirection first, EMovementDirection second) const {
	if ((first == EMovementDirection::LEFT && second == EMovementDirection::RIGHT) || 
		(second == EMovementDirection::LEFT && first == EMovementDirection::RIGHT)) {
		return true;
	} else if ((first == EMovementDirection::UP && second == EMovementDirection::DOWN) || 
		       (second == EMovementDirection::UP && first == EMovementDirection::DOWN)) {
		return true;
	}
	return false;
}

// проверка допустимо ли новое направление движения змейки 
bool ASnakeBase::IsValidNewDirection(EMovementDirection Direction) const {
	return !HasOppositeDirection(LastMoveDirection, Direction);
}

// пытаемся установить новое значение для направления змейки. если 
// такой поворот запрещён то функция вернет false и ничего не изменится
bool ASnakeBase::TrySetCurrentDirection(EMovementDirection Direction) {
	if (!IsValidNewDirection(Direction)) {
		return false;
	}
	CurrentMoveDirection = Direction;
	return true;
}

EMovementDirection ASnakeBase::GetCurrentDirection() const {
	return CurrentMoveDirection;
}